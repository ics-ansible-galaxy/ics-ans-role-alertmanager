# ics-ans-role-alertmanager

Ansible role to install alertmanager.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-alertmanager
```

## License

BSD 2-clause
